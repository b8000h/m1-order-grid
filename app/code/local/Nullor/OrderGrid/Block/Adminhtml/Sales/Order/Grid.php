<?php

// 有人此处是 extends Mage_Adminhtml_Block_Widget_Grid
// 然后余下内容全抄 Mage_Adminhtml_Block_Sales_Order_Grid，所以跟用 local/Mage/.. 的实现做法比并没干净多少
class Nullor_OrderGrid_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    // 因为这两个函数最末一行 是 parents::method()，没办法唯有全部复制，兼且修改最末一行
	protected function _prepareCollection()
	{
		// 此时获取的是 sales_flat_order_grid 的所有，所以无需在 sales_flat_order_grid 新建一列的
        // 如果已经在 sales_flat_order_grid 加了一列，然后在 _prepareCollection() 又通过 join() 添加的话，会报错如下：
        // Item (Mage_Sales_Model_Order) with the same id "196" already exist
        $collection = Mage::getResourceModel($this->_getCollectionClass());

        // 因为 sales_flat_order_grid 是节选了 sales_flat_order 中少量的几个 fields 过来，这里可以任意增加 fields
        $collection->join(array('order'=>'sales/order'),'main_table.entity_id=order.entity_id', array('customer_email', 'customer_group_id'));
        
        // 
        $collection->join(array('payment'=>'sales/order_payment'),'main_table.entity_id=payment.parent_id',array('method'));

        // 
        $collection->join(array('order_item'=>'sales/order_item'), 'main_table.entity_id=order_item.order_id', array('skus'  => new Zend_Db_Expr('group_concat(order_item.sku SEPARATOR ", ")')));

        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection(); // 注意这行你做了相应修改
	}
	
    protected function _prepareColumns()
    {

        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Purchased From (Store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
            ));
        }

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('shipping_name', array(
            'header' => Mage::helper('sales')->__('Ship to Name'),
            'index' => 'shipping_name',
        ));

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Base)'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ));

        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn('action',
                array(
                    'header'    => Mage::helper('sales')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => Mage::helper('sales')->__('View'),
                            'url'     => array('base'=>'*/sales_order/view'),
                            'field'   => 'order_id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
            ));
        }
        $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

        // 此后的 addColumn() 均为你后期添加的
        $this->addColumn('method', array(
            'header' => Mage::helper('sales')->__('Payment Method'),
            'index' => 'method',
        ));
        $this->addColumnsOrder('method', 'shipping_name');

        $this->addColumn('skus', array(
            'header' => Mage::helper('sales')->__('SKUs'),
            'index' => 'skus',
        ));
        $this->addColumnsOrder('skus', 'shipping_name');

        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }
}